---
layout: page
title: Diet
---

I react badly to short-chain carbohydrates, so I should not eat:

Vegetables
- onions
- garlic
- cabbage
- artichokes
- shallots
- leeks
- asparagus
- broccoli
- Brussels sprouts
- sugar snap peas
- cauliflower
- mushrooms
- sweet corn
- sweet potato

Grains
- wheat
- rye
- barley

Legumes & Nuts
- chickpeas
- lentils
- beans (all)

Dairy
- cow's milk


But all is not lost! I can have:

Vegetables
- alfalfa
- bean sprouts
- pak choi
- butternut squash
- carrots
- celeriac
- courgette
- cucumber
- aubergine
- fennel
- bell peppers
- green beans
- kale
- lettuce
- okra
- olives
- parsnip
- potatoes
- pumpkin
- radish
- spinach
- swede
- Swiss chard
- tomatoes
- turnip

Grains, nuts and seeds
- buckwheat
- rice
- millet
- maize
- oats
- quinoa
- polenta
- tapioca

Legumes & Nuts
- Brazil nuts
- chestnuts
- hazelnuts
- macadamia nuts
- peanuts
- pecans
- walnuts
- chia seeds
- poppy seeds
- pumpkin seeds
- sesame seeds
- sunflower seeds

Dairy
- milk alternatives such as: almond milk, rice milk, lactose free milk

I can also have all meats, seafood, eggs, and dairy cheese.
